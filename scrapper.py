#!/usr/bin/python3

from __future__ import print_function

try:
    # For Python 3.0 and later
    from urllib.request import urlopen
except ImportError:
    from urllib2 import urlopen

import bs4
import xlwt

try:
	import urllib.parse as urlparse
except ImportError:
	import urlparse

url = 'https://www.ted.com/talks/quick-list'

key_page = 'page'
page_start_value = 1

def fetch_ted_talks(page):
	page_url = url + '?' + key_page + '=' + str(page)
	print ('Scrapping', page_url)
	response = urlopen(page_url)
	data = response.read().decode('utf-8', 'ignore')
	soup = bs4.BeautifulSoup(data, 'html.parser')

	quick_list_container = soup.select('div.quick-list__container')
	if len(quick_list_container) != 1:
		return []

	items = quick_list_container[0].select('div.quick-list__container-row')
	if len(items) == 0:
		return []

	fetched_list = []
	for item in items:
		row = item.select('div.quick-list__row')[0]
		published = row.select('div.col-xs-1')[0].text.strip()
		full_title = row.select('div.title')[0].text.strip()
		author = full_title.split(':')[0].strip()
		title = full_title.split(':')[1].strip()
		talk_link = row.select('div.title')[0].select('a')[0].attrs.get('href')
		talk_link = urlparse.urljoin(url, talk_link)
		event = row.select('div.event')[0].text.strip()
		event_link = row.select('div.event')[0].select('a')[0].attrs.get('href')
		event_link = urlparse.urljoin(url, event_link)
		duration = row.select('div.col-xs-1')[1].text.strip()

		links = row.select('div.col-xs-2')[1]\
					.select('a')

		low_link = None
		medium_link = None
		high_link = None
		if len(links) == 3:
			low_link = links[0].attrs.get('href')
			medium_link = links[1].attrs.get('href')
			high_link = links[2].attrs.get('href')

		item = (published, author, title, talk_link, event, event_link, duration, low_link, medium_link, high_link)

		fetched_list.append(item)

	return fetched_list

fetched_list = [None]
total_list = []
current_page = page_start_value
while len(fetched_list) != 0:
	fetched_list = fetch_ted_talks(current_page)

	if len(fetched_list) == 0:
		continue

	total_list += fetched_list
	current_page += 1

print ('Saving to excel document')
book = xlwt.Workbook(encoding="utf-8")
sheet1 = book.add_sheet("Sheet 1")
for i in range(len(total_list)):
	for j in range(len(total_list[i])):
		sheet1.write(i, j, total_list[i][j])

book.save('result.xls')